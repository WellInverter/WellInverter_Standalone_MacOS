## WellInverter Standalone version for Max OS X

Standalone package of the WellInverter software.

- **64bits version**: [WellInverter_Standalone_MacOS_x86-64.zip](WellInverter_Standalone_MacOS_x86-64.zip)
- **no 32bits version**

---

The zip file is password protected. Please ask the authors for password:

[https://team.inria.fr/ibis/wellinverter/](https://team.inria.fr/ibis/wellinverter/)

---

##### Install

First, check that you have a recent java version (>7) installed in your system.
If not, install the latest [Java Runtime Environment](https://www.java.com/en/download/)

1. Unzip the file into a directory inside which WellInverter is to be installed
2. Open a console in this directory
3. Type `./installWellInverter.sh`
4. Answer to the questions during install procedure
5. WellInverter should now be installed

---

##### Use

1. Open a console inside the WellInverter directory
2. Type `./startWellInverter.sh`
3. Allow the application to access to the network (if asked by your firewall or antivirus)
4. Browse to [http://localhost:8000](http://localhost:8000)
5. Use WellInverter
6. Close the console after finishing the use of WellInverter

---

